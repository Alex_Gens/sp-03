package ru.kazakov.iteco.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;
import ru.kazakov.iteco.enumeration.Status;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class ProjectDTO {

    @NotNull
    private String id;

    @Nullable
    private String name;

    @NotNull
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateCreate;

    @Nullable
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateStart;

    @Nullable
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @Nullable
    private String description;

    @Override
    public String toString() {
        return name;
    }

}
