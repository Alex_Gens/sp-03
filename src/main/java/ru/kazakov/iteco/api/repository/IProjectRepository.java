package ru.kazakov.iteco.api.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.kazakov.iteco.model.Project;

public interface IProjectRepository extends PagingAndSortingRepository<Project, String> {}
